# Taller de pruebas

Para este ejercicio usted tendrá que modificar los archivos base para
obtener los resultados esperados segun las ejecuciones del **index.php** y verificará si existen inconsistencias con los requeriminetos (y las reparará de ser así).

1. **Entidades:** (30%)
    * Todos los aliens tienen los siguientes atributos: `nombre, edad, especie,
    planeta de origen y moral`, la cual por defecto es **"neutral"**.

    * Todos los aliens usan como medio de comunicación la telepatía, por eso usted
    deberá definir una constante de clase `COMUNICACION = "telepaticamente"`

    * Todos los aliens tienen un método llamado interact, el cual varía según el
    tipo de alien, los aliens buenos retornan `[COMUNICACION] dice: Hola 
    terricola mi nombre es [NOMBRE], vinimos en son de paz`, mientras los aliens
    malos retornan `[COMUNICACION] dice: Hola terricola, rindanse ante la 
    invasión de [NOMBRE]`

    * Todos los aliens tiene un método whoIAm que retorna: `[COMICACION] dice: 
    Mi nombre es [NOMBRE], vengo del plantea [PLANETA], soy un [ESPECIE] y soy [MORAL]`

    * Los aliens **buenos** vienen de Marte, Luna y Venus. Su moral es "bueno"
    * Los aliens **malos** vienen de Plutón, Jupiter y Saturno. Su moral es "malo"

    * Los aliens de **Jupiter** son los unicos que hablan con acento (telepaticamente) 
    así que todas las "s" las pronuncian como "sh"

    * Los aliens de **Venus** gimen (telepaticamente) despues de cada ","
    así que todas las "," se reemplazan por ", hmmm,"

    * Todos los aliens de un planeta específico no requiren de un planeta en su
    constructor, **ej:** los MarsAlien tienen que ser todos de marte.

    * **Los aliens buenos** deben tener los siguientes métodos:
     - `salvarPlaneta(Planeta planeta)`, cambia el estado del planeta
     - `llamarACasa()`, retorna "Llamando a Caaaaasaaaaa"

    * **Los aliens malos** deben tener los siguientes métodos:
     - `destruirPlaneta(Planeta planeta)`, cambia el estado del planeta
     - `pedirRefuerzos()`, retorna "Solicitando refuerzos Muahahaha"

    * Todos los atributos de los aliens deben estar encapsulados
    * Se require una fabrica abstracta de aliens AlienFactory
    
    * El planeta tiene un nombre y estado "a salvo" o "destruido"; siempre
    un planeta está "a salvo" a menos que lo destruyan.
    * El planeta tiene un método status que retorna su estado
    * Todos los atributos de los planetas deben estar ecapsulados.

2. **Pruebas:** (70%)
	* Escriba pruebas de unidad para cada una de las entidades generadas que prueben todos los métodos diferentes a los Getters y Setters.
	* Cree una prueba funcional que pruebe que el contenido de index.html (en cuanto a el procedimiento lógico) es correcto.
  