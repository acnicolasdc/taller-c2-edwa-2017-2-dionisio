<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GoodAlien
 *
 * @author pabhoz
 */
abstract class GoodAlien extends Alien implements IGoodAlien{
    
    protected $moral = "bueno";
    
    public function salvarPlaneta(Planet $planeta){
        $planeta->setEstado("a salvo");
    }
    public function llamarACasa(){
        return "Llamando a Caaaaasaaaaa";
    }
}
