<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Planet
 *
 * @author pabhoz
 */
class Planet {
    private $nombre;
    private $estado = "a salvo";
    
    function __construct($nombre) {
        $this->nombre = $nombre;    
    }

    function getNombre() {
        return $this->nombre;
    }

    function getEstado() {
        return $this->estado;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    public function status(){
        return $this->estado;
    }
}
